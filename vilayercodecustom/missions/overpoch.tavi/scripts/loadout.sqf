//________________________________start_loadout_______________________________
private ["_isSet","_puid"];
 
// Get player uid.
if (!isDedicated) then {
    waitUntil {(getPlayerUID player) != ""};
    _puid = getPlayerUID player;
    //diag_log ("Player UID retrieved.");
}
else {
    // not player so set to empty.
    _puid = "";
};
_isSet = false;
 
	// DO NOT EDIT ABOVE THIS LINE
 
// Admin Pickle
if ((_puid == "76561197998529422")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Hendo
if ((_puid == "76561197996504078")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin MeDzRoNmE
if ((_puid == "76561198029326998")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Sixkiller
if ((_puid == "76561197966888929")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Pinakion
if ((_puid == "76561198051182835")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Greg
if ((_puid == "76561198068478181")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Fistytheclown
if ((_puid == "76561198023003342")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","ItemRadio"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Chicky
if ((_puid == "76561197998562905")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_SurvivorWpink_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin ph4se
if ((_puid == "76561197961841633")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Admin Obi Wan
if ((_puid == "76561198019063521")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Soldier1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Subben
if ((_puid == "76561197960749891")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Rich2703
if ((_puid == "76561198084169156")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Allanon
if ((_puid == "76561198121869562")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members SoLo008
if ((_puid == "76561198060686388")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Krejven
if ((_puid == "76561197961847973")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members RONNIETHEBIGMAN
if ((_puid == "76561198042324138")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members JoeTurmoil
if ((_puid == "76561198152207336")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Camo1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","Binocular"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Thiriel
if ((_puid == "76561198060339022")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Sequenced
if ((_puid == "76561198128079455")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Dark Demoniac
if ((_puid == "76561198007858089")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Dr Buckets
if ((_puid == "76561198033134299")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Member Donator BlackHawk
if ((_puid == "76561198127065101")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemBandage","ItemBandage","ItemPainkiller","ItemPainkiller","ItemMorphine","ItemBloodbag","FoodSteakCooked","FoodSteakCooked","ItemWaterbottle","ItemWaterbottle","ItemAntibiotic","Skin_Camo1_DZ"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap","ItemWatch","ItemKnife","ItemMatchbox","ItemGPS","ItemCrowbar","Binocular"];
                DefaultBackpack = "DZ_Backpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Dobson
if ((_puid == "76561198065563811")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Brian
if ((_puid == "76561198141705111")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};

// Members Firedcylinder
if ((_puid == "76561197972425792")) then {
                DefaultMagazines = ["ItemBandage","ItemBandage","ItemPainkiller","ItemMorphine","FoodCanBakedBeans","ItemSodaMdew"];
                DefaultWeapons = ["ItemFlashlight","ItemToolbox","ItemCompass","ItemMap"];
                DefaultBackpack = "DZ_CivilBackpack_EP1";
                DefaultBackpackWeapon = "";
    _isSet = true;
};
	
	// Default
if (!(_isSet)) then {
		DefaultMagazines = ["ItemPainkiller","ItemBandage"];
		DefaultWeapons = ["ItemToolbox","ItemFlashlight","ItemCompass"];
		DefaultBackpack = "DZ_Patrol_Pack_EP1";
		DefaultBackpackItems = "";
};
//_______________________________finish _loadout____________________________
